<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BackupCaja
 *
 * @author Irene
 */
include_once 'caja.php';

class BackupCaja extends Caja {

    private $fechaVenta;
    private $lejaOcupada;
    private $codigoEstanteriaBackup;

    function __construct($codigoCaja, $color, $anchura, $altura, $profundidad, $material, $contenido, $fechaAlta, $fechaVenta, $lejaOcupada, $codigoEstanteriaBackup) {

        parent::__construct($codigoCaja, $color, $anchura, $altura, $profundidad, $material, $contenido, $fechaAlta);
        $this->fechaVenta = $fechaVenta;
        $this->lejaOcupada = $lejaOcupada;
        $this->codigoEstanteriaBackup = $codigoEstanteriaBackup;
    }
    
    function getFechaVenta() {
        return $this->fechaVenta;
    }

    function getLejaOcupada() {
        return $this->lejaOcupada;
    }

    function getCodigoEstanteriaBackup() {
        return $this->codigoEstanteriaBackup;
    }

    function setFechaVenta($fechaVenta) {
        $this->fechaVenta = $fechaVenta;
    }

    function setLejaOcupada($lejaOcupada) {
        $this->lejaOcupada = $lejaOcupada;
    }

    function setCodigoEstanteriaBackup($codigoEstanteriaBackup) {
        $this->codigoEstanteriaBackup = $codigoEstanteriaBackup;
    }

}
