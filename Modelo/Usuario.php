<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Usuario
 *
 * @author Irene
 */
class Usuario {
    private $usuario;
    private $nombreUsuario;
    private $apellidoUsuario;
    private $passUsuario;
    
    function __construct($usuario, $passUsuario) {
        $this->usuario = $usuario;
        $this->passUsuario = $passUsuario;
    }
    
    function getUsuario() {
        return $this->usuario;
    }

    function getNombreUsuario() {
        return $this->nombreUsuario;
    }

    function getApellidoUsuario() {
        return $this->apellidoUsuario;
    }

    function getPassUsuario() {
        return $this->passUsuario;
    }

    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    function setNombreUsuario($nombreUsuario) {
        $this->nombreUsuario = $nombreUsuario;
    }

    function setApellidoUsuario($apellidoUsuario) {
        $this->apellidoUsuario = $apellidoUsuario;
    }

    function setPassUsuario($passUsuario) {
        $this->passUsuario = $passUsuario;
    }



}
