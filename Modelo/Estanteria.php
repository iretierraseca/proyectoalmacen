<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Estanteria
 *
 * @author Irene
 */

include_once '../Modelo/ContenedorEstanteria.php';

class Estanteria extends ContenedorEstanteria{
    //put your code here
    private $idEstanteria;
    
    private $numeroDeLejas;
    private $pasillo;
    private $numero;
    private $lejasOcupadas;
    
    function __construct( $codigoEstanteria, $material, $numeroDeLejas, $pasillo, $numero) {
        
        parent::__construct($codigoEstanteria, $material);
        
        $this->numeroDeLejas = $numeroDeLejas;
        $this->pasillo = $pasillo;
        $this->numero = $numero;
        $this->lejasOcupadas = 0 ;
        
    }
    
    public function __toString() {
        
        return "Objeto Estanteria: <br>" . "codigo estanteria: " . $this->codigoEstanteria . " material: " . $this->material . " numero de lejas: " .$this->numeroDeLejas ."<br>"
                . "Ubicación: " . "Pasillo: " . $this->pasillo . "numero: " . $this->numero;
    }
    
    //GETTER
    
    function getIdEstanteria() {
        return $this->idEstanteria;
    }

    function getNumeroDeLejas() {
        return $this->numeroDeLejas;
    }

    function getPasillo() {
        return $this->pasillo;
    }

    function getNumero() {
        return $this->numero;
    }

    function getLejasOcupadas() {
        return $this->lejasOcupadas;
    }
    
    //SETTER

    function setIdEstanteria($idEstanteria) {
        $this->idEstanteria = $idEstanteria;
    }

    function setNumeroDeLejas($numeroDeLejas) {
        $this->numeroDeLejas = $numeroDeLejas;
    }

    function setPasillo($pasillo) {
        $this->pasillo = $pasillo;
    }

    function setNumero($numero) {
        $this->numero = $numero;
    }

    function setLejasOcupadas($lejasOcupadas) {
        $this->lejasOcupadas = $lejasOcupadas;
    }


    
}
