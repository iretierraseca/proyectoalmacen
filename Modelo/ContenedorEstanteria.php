<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ContenedorEstanteria
 *
 * @author Irene
 */


class ContenedorEstanteria {
    //put your code here
    private $codigoEstanteria;//codigoContenedor
    private $material;
    
    function __construct($codigoEstanteria, $material) {
        $this->codigoEstanteria = $codigoEstanteria;
        $this->material = $material;
    }

    
    function setCodigoEstanteria($codigoEstanteria) {
        
        $this->codigoEstanteria = $codigoEstanteria;
    }

    function setMaterial($material) {
        $this->material = $material;
    }
    
    function getCodigoEstanteria() {
        return $this->codigoEstanteria;
    }

    function getMaterial() {
        return $this->material;
    }


    
}
