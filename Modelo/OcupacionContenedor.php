<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OcupacionContenedor
 *
 * @author Irene
 */
class OcupacionContenedor {
    //put your code here
    private $idOcupacionContenedor;
    private $idCajaOcupacionContenedor;
    private $idContenedorOcupacion;
    
    function __construct($idOcupacionContenedor, $idCajaOcupacionContenedor, $idContenedorOcupacion) {
        $this->idOcupacionContenedor = $idOcupacionContenedor;
        $this->idCajaOcupacionContenedor = $idCajaOcupacionContenedor;
        $this->idContenedorOcupacion = $idContenedorOcupacion;
        
    }
    
    function getIdOcupacionContenedor() {
        return $this->idOcupacionContenedor;
    }

    function getIdCajaOcupacionContenedor() {
        return $this->idCajaOcupacionContenedor;
    }

    function getIdContenedorOcupacion() {
        return $this->idContenedorOcupacion;
    }

    function setIdOcupacionContenedor($idOcupacionContenedor) {
        $this->idOcupacionContenedor = $idOcupacionContenedor;
    }

    function setIdCajaOcupacionContenedor($idCajaOcupacionContenedor) {
        $this->idCajaOcupacionContenedor = $idCajaOcupacionContenedor;
    }

    function setIdContenedorOcupacion($idContenedorOcupacion) {
        $this->idContenedorOcupacion = $idContenedorOcupacion;
    }



    
}
