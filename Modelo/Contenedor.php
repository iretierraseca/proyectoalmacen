<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Contenedor
 *
 * @author Irene
 */

include_once '../Modelo/ContenedorEstanteria.php';

class Contenedor extends ContenedorEstanteria{
    
    private $idContenedor;
    private $capacidadContenedor;
    private $numOcupados;
    
    //$codigoEstanteria == codigoContenedor
    
    function __construct($idContenedor, $capacidadContenedor, $numOcupados, $codigoEstanteria, $material) {
        parent::__construct($codigoEstanteria, $material);
        $this->idContenedor = $idContenedor;
        $this->capacidadContenedor = $capacidadContenedor;
        $this->numOcupados = $numOcupados;
    }
    
    function getIdContenedor() {
        return $this->idContenedor;
    }

    function getCapacidadContenedor() {
        return $this->capacidadContenedor;
    }
    
    function getNumOcupados() {
        return $this->numOcupados;
    }

    
    function setIdContenedor($idContenedor) {
        $this->idContenedor = $idContenedor;
    }

    function setCapacidadContenedor($capacidadContenedor) {
        $this->capacidadContenedor = $capacidadContenedor;
    }
    
    function setNumOcupados($numOcupados) {
        $this->numOcupados = $numOcupados;
    }





    
}
