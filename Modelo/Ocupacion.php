<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Ocupacion
 *
 * @author Irene
 */
class Ocupacion {
    private $idOcupacion;
    private $idCajaOcupacion;
    private $idEstanteriaOcupacion;
    private $lejaOcupada;
    
    function __construct($idCajaOcupacion, $idEstanteriaOcupacion, $lejaOcupada) {
        $this->idCajaOcupacion = $idCajaOcupacion;
        $this->idEstanteriaOcupacion = $idEstanteriaOcupacion;
        $this->lejaOcupada = $lejaOcupada;
    }
    
    
    function getIdOcupacion() {
        return $this->idOcupacion;
    }

    function getIdCajaOcupacion() {
        return $this->idCajaOcupacion;
    }

    function getIdEstanteriaOcupacion() {
        return $this->idEstanteriaOcupacion;
    }

    function getLejaOcupada() {
        return $this->lejaOcupada;
    }

    function setIdOcupacion($idOcupacion) {
        $this->idOcupacion = $idOcupacion;
    }

    function setIdCajaOcupacion($idCajaOcupacion) {
        $this->idCajaOcupacion = $idCajaOcupacion;
    }

    function setIdEstanteriaOcupacion($idEstanteriaOcupacion) {
        $this->idEstanteriaOcupacion = $idEstanteriaOcupacion;
    }

    function setLejaOcupada($lejaOcupada) {
        $this->lejaOcupada = $lejaOcupada;
    }

}
