<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Caja
 *
 * @author Irene
 */
class Caja {
    
    private $idCaja;
    private $codigoCaja;
    private $color;
    private $anchura;
    private $altura;
    private $profundidad;
    private $material;
    private $contenido;
    private $fechaAlta;
    
    function __construct($codigoCaja, $color, $anchura, $altura, $profundidad, $material, $contenido, $fechaAlta) {
        $this->codigoCaja = $codigoCaja;
        $this->color = $color;
        $this->anchura = $anchura;
        $this->altura = $altura;
        $this->profundidad = $profundidad;
        $this->material = $material;
        $this->contenido = $contenido;
        $this->fechaAlta = $fechaAlta;
    }

    public function __toString() {
        $caja = "Codigo: " . $this->codigoCaja .
                " Color: " . $this->color .
                " Anchura: " . $this->anchura .
                " Altura: " . $this->altura .
                " Profundidad: " . $this->profundidad . 
                " Material: " . $this->material . 
                " Contenido: " . $this->contenido . 
                " Fecha Alta: " . $this->fechaAlta; 
                
        return $caja;
    }
    
    function getIdCaja() {
        return $this->idCaja;
    }

    function getCodigoCaja() {
        return $this->codigoCaja;
    }

    function getColor() {
        return $this->color;
    }

    function getAnchura() {
        return $this->anchura;
    }

    function getAltura() {
        return $this->altura;
    }

    function getProfundidad() {
        return $this->profundidad;
    }

    function getMaterial() {
        return $this->material;
    }

    function getContenido() {
        return $this->contenido;
    }

    function getFechaAlta() {
        return $this->fechaAlta;
    }

    function setIdCaja($idCaja) {
        $this->idCaja = $idCaja;
    }

    function setCodigoCaja($codigoCaja) {
        $this->codigoCaja = $codigoCaja;
    }

    function setColor($color) {
        $this->color = $color;
    }

    function setAnchura($anchura) {
        $this->anchura = $anchura;
    }

    function setAltura($altura) {
        $this->altura = $altura;
    }

    function setProfundidad($profundidad) {
        $this->profundidad = $profundidad;
    }

    function setMaterial($material) {
        $this->material = $material;
    }

    function setContenido($contenido) {
        $this->contenido = $contenido;
    }

    function setFechaAlta($fechaAlta) {
        $this->fechaAlta = $fechaAlta;
    }



    
}
