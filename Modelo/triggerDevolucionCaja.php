<?php

$sqltb = ("DROP TRIGGER IF EXISTS devolucionCaja");
$resul = $conexion->query($sqltb);
$codeErrorDrop = $conexion->errno;

//SACAR LAS VARIABLES CON METODOS ->GET

$codigoCaja = $_objCaja->getCodigoCaja();
$color = $_objCaja->getColor();
$anchura = $_objCaja->getAnchura();
$altura = $_objCaja->getAltura();
$profundidad = $_objCaja->getProfundidad();
$material = $_objCaja->getMaterial();
$contenido = $_objCaja->getContenido();
$fecha = $_objCaja->getFechaAlta();

$sqltrig = ("CREATE DEFINER=`root`@`localhost` TRIGGER `devolucionCaja` BEFORE DELETE ON `backupcaja` FOR EACH ROW BEGIN
INSERT INTO caja
(codigoCaja,color,anchura,altura,profundidad,material,contenido,fechaAlta)
VALUES 
('".$codigoCaja."',
'".$color."',
$anchura,
$altura,
$profundidad,
'".$material."',
'".$contenido."',
'".$fecha."'
);
INSERT INTO ocupacion
(idCajaOcupacion,idEstanteriaOcupacion,lejaOcupada)
VALUES
((SELECT idCaja FROM caja WHERE codigoCaja='" . $codigoCaja . "'),
$idEstanteria,
$lejaOcupada
);
UPDATE estanteria SET lejasOcupadas = lejasOcupadas +1 WHERE idEstanteria=$idEstanteria;
END;");

$res = $conexion->query($sqltrig);
$codeErrorTrig = $conexion->errno;

