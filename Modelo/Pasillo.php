<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pasillo
 *
 * @author Irene
 */
class Pasillo {
    private $idPasillo;
    private $letra;
    private $numOcupadas;
    
    function __construct($letra) {
        $this->letra = $letra;
        $this->numOcupadas = 0;
    }
    
    function getIdPasillo() {
        return $this->idPasillo;
    }

    function getLetra() {
        return $this->letra;
    }

    function getNumOcupadas() {
        return $this->numOcupadas;
    }

    function setIdPasillo($idPasillo) {
        $this->idPasillo = $idPasillo;
    }

    function setLetra($letra) {
        $this->letra = $letra;
    }

    function setNumOcupadas($numOcupadas) {
        $this->numOcupadas = $numOcupadas;
    }

    public function __toString() {
        return "letra:" . $this->letra . " ocupadas: " . $this->numOcupadas ;
    }
}
