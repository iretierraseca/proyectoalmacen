<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ExceptionGeneral
 *
 * @author Irene
 */
class ExceptionGeneral extends Exception {
    private $sitio;
     public function __construct($_message, $_code, $sitio) {
        
        parent:: __construct($_message, $_code , null);
        $this->sitio = $sitio;
            
    }
    public function __toString() {
        return __CLASS__ . ": " .$this->code ." ". $this->message  . " En el método " . $this->sitio ;
        
    }
    
    function getSitio() {
        return $this->sitio;
    }
    
    function getClass(){
        return __CLASS__;
    }


}
