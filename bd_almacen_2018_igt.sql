-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.23-log - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para bd_almacen_igt_2019_examen
DROP DATABASE IF EXISTS `bd_almacen_igt_2019_examen`;
CREATE DATABASE IF NOT EXISTS `bd_almacen_igt_2019_examen` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `bd_almacen_igt_2019_examen`;

-- Volcando estructura para tabla bd_almacen_igt_2019_examen.backupcaja
DROP TABLE IF EXISTS `backupcaja`;
CREATE TABLE IF NOT EXISTS `backupcaja` (
  `idBackupcaja` int(11) NOT NULL AUTO_INCREMENT,
  `codigoCajaBackup` varchar(5) NOT NULL,
  `color` varchar(7) NOT NULL,
  `anchura` int(5) NOT NULL,
  `altura` int(5) NOT NULL,
  `profundidad` int(5) NOT NULL,
  `material` varchar(35) NOT NULL,
  `contenido` varchar(50) NOT NULL,
  `fechaAlta` date NOT NULL,
  `fechaVenta` date NOT NULL,
  `lejaOcupada` int(5) NOT NULL,
  `codigoEstanteriaBackup` varchar(5) NOT NULL,
  PRIMARY KEY (`idBackupcaja`),
  UNIQUE KEY `codigoCajaBackup` (`codigoCajaBackup`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bd_almacen_igt_2019_examen.backupcaja: ~0 rows (aproximadamente)
DELETE FROM `backupcaja`;
/*!40000 ALTER TABLE `backupcaja` DISABLE KEYS */;
/*!40000 ALTER TABLE `backupcaja` ENABLE KEYS */;

-- Volcando estructura para tabla bd_almacen_igt_2019_examen.caja
DROP TABLE IF EXISTS `caja`;
CREATE TABLE IF NOT EXISTS `caja` (
  `idCaja` int(11) NOT NULL AUTO_INCREMENT,
  `codigoCaja` varchar(5) NOT NULL,
  `color` varchar(20) NOT NULL,
  `anchura` int(11) NOT NULL,
  `altura` int(11) NOT NULL,
  `profundidad` int(11) NOT NULL,
  `material` varchar(50) NOT NULL,
  `contenido` varchar(50) NOT NULL,
  `fechaAlta` date DEFAULT NULL,
  PRIMARY KEY (`idCaja`),
  UNIQUE KEY `codigoCaja` (`codigoCaja`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bd_almacen_igt_2019_examen.caja: ~1 rows (aproximadamente)
DELETE FROM `caja`;
/*!40000 ALTER TABLE `caja` DISABLE KEYS */;
INSERT INTO `caja` (`idCaja`, `codigoCaja`, `color`, `anchura`, `altura`, `profundidad`, `material`, `contenido`, `fechaAlta`) VALUES
	(51, 'CA001', '#000000', 10, 10, 10, 'Material', 'contenido', '2019-01-18');
/*!40000 ALTER TABLE `caja` ENABLE KEYS */;

-- Volcando estructura para tabla bd_almacen_igt_2019_examen.contenedor
DROP TABLE IF EXISTS `contenedor`;
CREATE TABLE IF NOT EXISTS `contenedor` (
  `idContenedor` int(11) NOT NULL AUTO_INCREMENT,
  `codigoContenedor` varchar(5) NOT NULL,
  `materialContenedor` varchar(50) NOT NULL,
  `capacidadContenedor` int(11) NOT NULL DEFAULT '5',
  `numOcupados` int(11) NOT NULL,
  PRIMARY KEY (`idContenedor`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bd_almacen_igt_2019_examen.contenedor: ~2 rows (aproximadamente)
DELETE FROM `contenedor`;
/*!40000 ALTER TABLE `contenedor` DISABLE KEYS */;
INSERT INTO `contenedor` (`idContenedor`, `codigoContenedor`, `materialContenedor`, `capacidadContenedor`, `numOcupados`) VALUES
	(1, 'CO001', 'Aluminio', 5, 1),
	(2, 'CO002', 'Aluminio', 5, 0);
/*!40000 ALTER TABLE `contenedor` ENABLE KEYS */;

-- Volcando estructura para tabla bd_almacen_igt_2019_examen.empresa
DROP TABLE IF EXISTS `empresa`;
CREATE TABLE IF NOT EXISTS `empresa` (
  `idEmpresa` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) NOT NULL,
  `cif` varchar(9) NOT NULL,
  `direccion` varchar(30) NOT NULL,
  `telefono` varchar(9) NOT NULL,
  `email` varchar(30) NOT NULL,
  `numeroTotal` int(5) NOT NULL,
  PRIMARY KEY (`idEmpresa`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bd_almacen_igt_2019_examen.empresa: ~0 rows (aproximadamente)
DELETE FROM `empresa`;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
INSERT INTO `empresa` (`idEmpresa`, `nombre`, `cif`, `direccion`, `telefono`, `email`, `numeroTotal`) VALUES
	(1, 'kutxal', '48151818F', 'alfareros', '66608751', 'p13@gmail.com', 10);
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;

-- Volcando estructura para tabla bd_almacen_igt_2019_examen.estanteria
DROP TABLE IF EXISTS `estanteria`;
CREATE TABLE IF NOT EXISTS `estanteria` (
  `idEstanteria` int(11) NOT NULL AUTO_INCREMENT,
  `codigoEstanteria` varchar(50) NOT NULL,
  `material` varchar(50) NOT NULL,
  `numeroDeLejas` int(11) NOT NULL,
  `idPasillo` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `lejasOcupadas` int(11) NOT NULL,
  PRIMARY KEY (`idEstanteria`),
  UNIQUE KEY `codigoEstanteria` (`codigoEstanteria`),
  UNIQUE KEY `idPasillo_numero` (`idPasillo`,`numero`),
  CONSTRAINT `FK_estanteria_pasillo` FOREIGN KEY (`idPasillo`) REFERENCES `pasillo` (`idPasillo`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bd_almacen_igt_2019_examen.estanteria: ~1 rows (aproximadamente)
DELETE FROM `estanteria`;
/*!40000 ALTER TABLE `estanteria` DISABLE KEYS */;
INSERT INTO `estanteria` (`idEstanteria`, `codigoEstanteria`, `material`, `numeroDeLejas`, `idPasillo`, `numero`, `lejasOcupadas`) VALUES
	(21, 'ES100', 'Carton', 10, 1, 6, 0);
/*!40000 ALTER TABLE `estanteria` ENABLE KEYS */;

-- Volcando estructura para tabla bd_almacen_igt_2019_examen.ocupacion
DROP TABLE IF EXISTS `ocupacion`;
CREATE TABLE IF NOT EXISTS `ocupacion` (
  `idOcupacion` int(11) NOT NULL AUTO_INCREMENT,
  `idCajaOcupacion` int(11) NOT NULL,
  `idEstanteriaOcupacion` int(11) NOT NULL,
  `lejaOcupada` int(11) NOT NULL,
  PRIMARY KEY (`idOcupacion`),
  UNIQUE KEY `idCaja` (`idCajaOcupacion`),
  UNIQUE KEY `idEstanteria_lejaOcupada` (`idEstanteriaOcupacion`,`lejaOcupada`),
  CONSTRAINT `FK_ocupacion_caja` FOREIGN KEY (`idCajaOcupacion`) REFERENCES `caja` (`idCaja`),
  CONSTRAINT `FK_ocupacion_estanteria` FOREIGN KEY (`idEstanteriaOcupacion`) REFERENCES `estanteria` (`idEstanteria`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bd_almacen_igt_2019_examen.ocupacion: ~0 rows (aproximadamente)
DELETE FROM `ocupacion`;
/*!40000 ALTER TABLE `ocupacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `ocupacion` ENABLE KEYS */;

-- Volcando estructura para tabla bd_almacen_igt_2019_examen.ocupacioncontenedor
DROP TABLE IF EXISTS `ocupacioncontenedor`;
CREATE TABLE IF NOT EXISTS `ocupacioncontenedor` (
  `idOcupacionContenedor` int(11) NOT NULL AUTO_INCREMENT,
  `idCajaOcupacionContenedor` int(11) NOT NULL,
  `idContenedorOcupacionContenedor` int(11) NOT NULL,
  PRIMARY KEY (`idOcupacionContenedor`),
  UNIQUE KEY `idCajaOcupacionContenedor` (`idCajaOcupacionContenedor`),
  UNIQUE KEY `idContenedorOcupacionContenedor` (`idContenedorOcupacionContenedor`),
  CONSTRAINT `FK_ocupacionContenedor_caja` FOREIGN KEY (`idCajaOcupacionContenedor`) REFERENCES `caja` (`idCaja`),
  CONSTRAINT `FK_ocupacionContenedor_contenedor` FOREIGN KEY (`idContenedorOcupacionContenedor`) REFERENCES `contenedor` (`idContenedor`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bd_almacen_igt_2019_examen.ocupacioncontenedor: ~0 rows (aproximadamente)
DELETE FROM `ocupacioncontenedor`;
/*!40000 ALTER TABLE `ocupacioncontenedor` DISABLE KEYS */;
INSERT INTO `ocupacioncontenedor` (`idOcupacionContenedor`, `idCajaOcupacionContenedor`, `idContenedorOcupacionContenedor`) VALUES
	(1, 51, 1);
/*!40000 ALTER TABLE `ocupacioncontenedor` ENABLE KEYS */;

-- Volcando estructura para tabla bd_almacen_igt_2019_examen.pasillo
DROP TABLE IF EXISTS `pasillo`;
CREATE TABLE IF NOT EXISTS `pasillo` (
  `idPasillo` int(11) NOT NULL AUTO_INCREMENT,
  `letra` varchar(1) NOT NULL,
  `numOcupadas` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idPasillo`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bd_almacen_igt_2019_examen.pasillo: ~4 rows (aproximadamente)
DELETE FROM `pasillo`;
/*!40000 ALTER TABLE `pasillo` DISABLE KEYS */;
INSERT INTO `pasillo` (`idPasillo`, `letra`, `numOcupadas`) VALUES
	(1, 'A', 1),
	(2, 'B', 0),
	(3, 'C', 0),
	(4, 'D', 0);
/*!40000 ALTER TABLE `pasillo` ENABLE KEYS */;

-- Volcando estructura para tabla bd_almacen_igt_2019_examen.usuario
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(50) NOT NULL,
  `nombreUsuario` varchar(50) NOT NULL,
  `apellidoUsuario` varchar(50) NOT NULL,
  `passUsuario` varchar(150) NOT NULL,
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bd_almacen_igt_2019_examen.usuario: ~1 rows (aproximadamente)
DELETE FROM `usuario`;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`idUsuario`, `usuario`, `nombreUsuario`, `apellidoUsuario`, `passUsuario`) VALUES
	(5, 'admin', 'Irene', 'Gomez ', '$2y$10$HCc1zcbvpAmrWSN/hrvpSOyACEWWzVsUs53A9WcstMx7NyXLoMVYu');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

-- Volcando estructura para disparador bd_almacen_igt_2019_examen.devolucionCaja
DROP TRIGGER IF EXISTS `devolucionCaja`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `devolucionCaja` BEFORE DELETE ON `backupcaja` FOR EACH ROW BEGIN
INSERT INTO caja
(codigoCaja,color,anchura,altura,profundidad,material,contenido,fechaAlta)
VALUES 
('CA001',
'#000000',
3,
3,
3,
'MAT',
'CONT',
'2018-11-26'
);
INSERT INTO ocupacion
(idCajaOcupacion,idEstanteriaOcupacion,lejaOcupada)
VALUES
((SELECT idCaja FROM caja WHERE codigoCaja='CA001'),
20,
2
);
UPDATE estanteria SET lejasOcupadas = lejasOcupadas +1 WHERE idEstanteria=20;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Volcando estructura para disparador bd_almacen_igt_2019_examen.ventaCaja
DROP TRIGGER IF EXISTS `ventaCaja`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `ventaCaja` BEFORE DELETE ON `ocupacion` FOR EACH ROW BEGIN
INSERT INTO ocupacioncontenedor
(idCajaOcupacionContenedor,idContenedorOcupacionContenedor)
VALUES 
((SELECT idCaja FROM caja WHERE codigoCaja='CA001'),
1
);
UPDATE estanteria SET lejasOcupadas = lejasOcupadas -1 WHERE idEstanteria=21;
UPDATE contenedor SET numOcupados = numOcupados +1 WHERE idContenedor=1;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
