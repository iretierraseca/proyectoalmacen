function muestraLejasLibres(str) {
    var xmlhttp = new XMLHttpRequest();
    
    if (str == "null") {
        document.getElementById("inputLejaCaja").innerHTML = "";
        document.getElementById("botonEnviarAltaCaja").setAttribute("disabled", "");
        return;
    } else {
        document.getElementById("botonEnviarAltaCaja").removeAttribute("disabled","");
    }

    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        /*Como el explorer va por su cuenta, el objeto es un ActiveX */
    }

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("inputLejaCaja").innerHTML = xmlhttp.responseText; /*Seleccionamos el elemento que recibirá el flujo de datos*/
        }
    }
    xmlhttp.open("GET", "../Controlador/getCajaLejasLibres.php?inputLejaCaja=" + str, true);
    /*Mandamos al PHP encargado de traer los datos, el valor de referencia */
    xmlhttp.send();

}


