<?php

include_once '../DAO/operaciones.php';
include_once '../Modelo/BackupCaja.php';
//include_once '../Modelo/Caja.php';
$key = $_REQUEST['key'];
$arrayObjCajas = operaciones::CajaDevolucion($key);

if (isset($_REQUEST['inputCodigoCaja'])) {
    session_start();
    $_SESSION['cajaBackupListadoAutocompletado'] = $arrayObjCajas;
    header('Location: ../Vista/vistaCajaBackupListadoAutocompletado.php');
} else {
    $key = $_REQUEST['valor'];
    $arrayObjCajas = operaciones::CajasBackupObtenerPorCodigo($key);
    session_start();
    $_SESSION['cajaBackupListadoAutocompletado'] = $arrayObjCajas;
    //print_r($arrayObjCajas);
    header('Location: getCajaBackupCodigoDevolucion.php');
    //print_r($arrayObjCajas);
}
?>
    