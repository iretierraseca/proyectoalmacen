<?php

include_once '../Modelo/Estanteria.php';

include_once '../Modelo/ContenedorEstanteria.php';

include_once '../DAO/operaciones.php';
include_once '../Modelo/ExceptionGeneral.php';

//Información recogida del formulario. vistaEstanteriaAlta
$codigoEstanteria = $_REQUEST['CodigoEstanteria'];
$material = $_REQUEST['MaterialEstanteria'];
$numeroDeLejas = $_REQUEST['NumLejasEstanteria'];
$pasillo = $_REQUEST['selectPasilloEstanteria'];
$numero = $_REQUEST['ListaNumeroEstanteria'];
//objeto estanteria
$objetoEstanteria = new Estanteria($codigoEstanteria, $material, $numeroDeLejas, $pasillo, $numero);

try {
    //Insertar estanteria 
    operaciones::EstanteriaInsertar($objetoEstanteria);
    header('Location:../Vista/vistaMensajeExito.php');
} catch (ExceptionGeneral $e) {
    $conexion->rollback();
    header('Location:../Vista/vistaMensajeError.php?mensaje='.$e);
}
?>