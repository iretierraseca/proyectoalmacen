<?php

include_once '../DAO/operaciones.php';
include_once '../Modelo/ExceptionGeneral.php';

try{
//obtener inventario
$inventario = operaciones::gestionInventario();
session_start();
$_SESSION['inventario']=$inventario;
header('Location: ../Vista/vistaGestionInventario.php');

}catch(ExceptionGeneral $e){
    header('Location: ../Vista/vistaMensajeError.php?mensaje='.$e);
}
 