<?php

include_once '../DAO/operaciones.php';
//include_once '../Modelo/Caja.php';
$key = $_REQUEST['key'];
$arrayObjCajas = operaciones::sugerenciaCaja($key);

if (isset($_REQUEST['inputCodigoCaja'])) {
    session_start();
    //sugerencias codigos
    $_SESSION['cajaListadoAutocompletado'] = $arrayObjCajas;
    header('Location: ../Vista/vistaCajaListadoAutocompletado.php');
} else {
    $key = $_REQUEST['valor'];
    //ObjetoCaja obtenida por codigo
    $arrayObjCajas = operaciones::CajasObtenerPorCodigo($key);
    session_start();
    $_SESSION['cajaListadoAutocompletado'] = $arrayObjCajas;
    header('Location: getCajaCodigoVenta.php');
}
?>
    