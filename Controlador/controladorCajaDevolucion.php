<?php

$codigoCaja = $_REQUEST['CodigoCaja'];
$color = $_REQUEST['ColorCaja'];
$anchura = $_REQUEST['AnchuraCaja'];
$altura = $_REQUEST['AlturaCaja'];
$profundidad = $_REQUEST['ProfundidadCaja'];
$material = $_REQUEST['MaterialCaja'];
$contenido = $_REQUEST['ContenidoCaja'];
$idEstanteriaOcupacion = $_REQUEST['selectInfoEstanteria'];
$lejaOcupada = $_REQUEST['inputLejaCaja'];
$fechaAlta = $_REQUEST['FechaCaja'];



include_once '../Modelo/Caja.php';
include_once '../Modelo/Ocupacion.php';
include_once '../DAO/operaciones.php';
include_once '../Modelo/ExceptionGeneral.php';

$objCaja = new Caja($codigoCaja, $color, $anchura, $altura, $profundidad, $material, $contenido, $fechaAlta);
$idCajaOcupacion = $objCaja->getIdCaja();

$objOcupacion = new Ocupacion($idCajaOcupacion, $idEstanteriaOcupacion, $lejaOcupada);

try {
    $conexion->autocommit(false); //transaccion
    operaciones::gestionDevolucion($objCaja, $objOcupacion);
    $conexion->commit();
    header('Location:../Vista/vistaMensajeExito.php');
} 
catch (ExceptionGeneral $e) {

    $conexion->rollback();
    header('Location:../Vista/vistaMensajeError.php?mensaje=' . $e);
}
?>