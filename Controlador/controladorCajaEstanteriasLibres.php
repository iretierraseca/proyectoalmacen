<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
session_start();

include_once '../DAO/operaciones.php';
include_once '../Modelo/Estanteria.php';
include_once '../Modelo/ExceptionGeneral.php';
/*Distinguir entre Alta caja y Devolucion caja*/
if(isset($_REQUEST['opcionMenu'])){
    if($_REQUEST['opcionMenu']=='AltaCaja'){
        $localizacion = 'Location:../Vista/vistaCajaAlta.php';
    }else if($_REQUEST['opcionMenu']=='DevolucionCaja'){
        //$localizacion = 'Location:../Vista/vistaCajaDevolucion.php';
        $localizacion = 'Location:controladorCajaExistenDatos.php?opcionLlamada=DevolucionCaja';
    }
}

//print_r($arrayObjEstanteriasLibres);

try {
    /*Obtener un array de estanterias las cuales esten libres*/
    $arrayObjEstanteriasLibres = operaciones::EstanteriasLibres();
    $_SESSION['EstanteriasLibres'] = $arrayObjEstanteriasLibres;
    header($localizacion);
} catch (ExceptionGeneral $e) {
    header('Location:../Vista/vistaMensajeError.php?mensaje=' . $e);
}
