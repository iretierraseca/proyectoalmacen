<?php

include_once '../DAO/operaciones.php';

$resultadoUsuarios = operaciones::inicio();

//Si no existen usuarios en la base de datos, se debe registrar
if(!$resultadoUsuarios ){ 
    header('Location: ../Vista/vistaAutenticacionRegistro.php');   
}else{ //Si ya existen usuarios, se debe logear
    header('Location: ../Vista/vistaAutenticacionIngreso.php');
}

?>