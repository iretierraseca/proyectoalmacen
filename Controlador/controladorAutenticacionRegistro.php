<?php

include_once '../Modelo/Usuario.php';
include_once '../DAO/operaciones.php';
include_once '../Modelo/ExceptionGeneral.php';

$usuario = $_REQUEST['Usuario'];
$nombreUsuario = $_REQUEST['NombreUsuario'];
$apellidoUsuario = $_REQUEST['ApellidoUsuario'];
$passUsuario = $_REQUEST['PassUsuario'];

//Creacion objeto usuario
$Usuario = new Usuario($usuario,$passUsuario);
$Usuario->setApellidoUsuario($apellidoUsuario);
$Usuario->setNombreUsuario($nombreUsuario);
try{
operaciones::registro($Usuario);
//Si el registro se ha efectuado correctamente se procede al login
header('Location:../Vista/vistaAutenticacionIngreso.php');
}catch(ExceptionGeneral $e){
    header('Location:../Vista/vistaMensajeError.php?mensaje='.$e);
}
//print_r($Usuario);
//echo $x;
?>
