<?php

//Inicio sesion
session_start();

include_once '../Modelo/Pasillo.php';
include_once '../DAO/operaciones.php';
include_once '../Modelo/ExceptionGeneral.php';

try {
    //Obtener las pasillos que tengan huecos libres para la colocación de una estantería
    $arrayObjPasillo = operaciones::EstanteriasPasillosLibres();
    $_SESSION['PasillosLibres'] = $arrayObjPasillo;
    header('Location:../Vista/vistaEstanteriaAlta.php');
} catch (ExceptionGeneral $e) {
    $conexion->rollback();
    header('Location:../Vista/vistaMensajeError.php?mensaje=' . $e);
}

//print_r($arrayObjPasillo);