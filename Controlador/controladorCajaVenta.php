<?php

include_once '../DAO/operaciones.php';
include_once '../Modelo/ExceptionGeneral.php';

$codigoCaja = $_REQUEST['CodigoCaja'];
$idContenedor = $_REQUEST['selectInfoControlador'];

$estanteria = $_REQUEST['inputEstanteriaCaja'];
//echo $codigoCaja;
try{
operaciones::CajasVentaConContenedor($codigoCaja, $idContenedor, $estanteria);

header('Location: ../Vista/vistaMensajeExito.php');
}catch(ExceptionGeneral $e){
 header('Location:../Vista/vistaMensajeError.php?mensaje=' . $e);   
}

?>