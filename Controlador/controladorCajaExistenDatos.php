<?php

session_start();
include_once '../DAO/operaciones.php';
include_once '../Modelo/ExceptionGeneral.php';
include_once '../Modelo/Contenedor.php';



/* Distinguir entre Alta caja y Devolucion caja */
if (isset($_REQUEST['opcionLlamada'])) {
    if ($_REQUEST['opcionLlamada'] == 'VentaCaja') {
        $funcion = "operaciones::CajasExisten()";
        $localizacion = 'Location:../Vista/vistaCajaVenta.php';
    } else if ($_REQUEST['opcionLlamada'] == 'DevolucionCaja') {
        //$localizacion = 'Location:../Vista/vistaCajaDevolucion.php';
        $funcion = "operaciones::CajasExistenBackup()";
        $localizacion = 'Location:../Vista/vistaCajaDevolucion.php';
    }
}

try {
    if ($_REQUEST['opcionLlamada'] == 'VentaCaja') {
        $existenCajas = operaciones::CajasExisten();
        //operacion sacar contenedores
        $array = operaciones::contenedoresLibres();
        $_SESSION['contenedoresLibres'] = $array;
    } else if ($_REQUEST['opcionLlamada'] == 'DevolucionCaja') {
        $existenCajas = operaciones::CajasExistenBackup();
    }
    header($localizacion);
} catch (ExceptionGeneral $e) {
    header('Location:../Vista/vistaMensajeError.php?mensaje=' . $e);
}