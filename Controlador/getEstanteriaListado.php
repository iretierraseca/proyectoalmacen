<?php

include_once '../DAO/operaciones.php';
include_once '../Modelo/ExceptionGeneral.php';
//Limite de registros por cada página
$limite = $_REQUEST['limite'];
if (isset($_REQUEST['pagina'])) {
    $pagina = $_REQUEST['pagina'];
} else {
    $pagina = 1;
}
$empezarDesde = ($pagina - 1) * $limite;

try {
    //Obtener un array de estanterias
    $estanterias = operaciones::EstanteriasListar($empezarDesde, $limite);
    session_start();
    $_SESSION['estanterias'] = $estanterias;
    header('Location:../Vista/vistaEstanteriaPaginacion.php');
} catch (ExceptionGeneral $e) {
    header('Location:../Vista/vistaMensajeInformacion.php?mensaje=' . $e);
}
?>