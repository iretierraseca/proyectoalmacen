<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of operaciones
 *
 * @author Irene
 */
include_once 'ConexionBDPHP.php';

class operaciones {
    //ESTANTERIAS

    /**
     * Call: Controlador/controladorEstanteriaAlta.php
     * 
     * @global ConexionBDPHP $conexion
     * @param Estanteria $_objetoEstanteria
     * @throws ExceptionGeneral
     * 
     * Insertar estanteria
     * Actualizar campo numOcupadas(número de huecos en un pasillo) de la tabla pasillo
     */
    public static function EstanteriaInsertar($_objetoEstanteria) {

        global $conexion;

        //Variables objeto estanteria
        $codigoEstanteria = $_objetoEstanteria->getCodigoEstanteria();
        $material = $_objetoEstanteria->getMaterial();
        $numeroDeLejas = $_objetoEstanteria->getNumeroDeLejas();
        $pasillo = $_objetoEstanteria->getPasillo();
        $numero = $_objetoEstanteria->getNumero();
        $lejasOcupadas = $_objetoEstanteria->getLejasOcupadas();

        //Deshabilita la confirmación automatica
        $conexion->autocommit(false);

        //INSERTAR ESTANTERIA
        $consulta = $conexion->prepare("INSERT INTO estanteria (codigoEstanteria,material,numeroDeLejas,idPasillo,numero,lejasOcupadas) VALUES (?,?,?,?,?,?)");
        $consulta->bind_param("ssiiii", $codigoEstanteria, $material, $numeroDeLejas, $pasillo, $numero, $lejasOcupadas);
        $resultadoInsertar = $consulta->execute();
        $codeErrorInsertar = $conexion->errno;
        $mensajeErrorInsertar = $conexion->error;

        //Si falla la insercción
        if (!$resultadoInsertar) {
            throw new ExceptionGeneral($mensajeErrorInsertar, $codeErrorInsertar, __FUNCTION__);
        }

        //ACTUALIZAR NUMERO DE HUECOS EN UN PASILLO
        $stmtActualizarPasillo = $conexion->prepare("UPDATE pasillo SET numOcupadas = numOcupadas + 1 WHERE idPasillo=?");
        $stmtActualizarPasillo->bind_param('i', $pasillo);
        $stmtActualizarPasillo->execute();
        $mensajeErrorActualizar = 'No se ha podido realizar la actualización';
        $codeErrorActualizar = '1000';

        //Si falla la actualización
        if ($stmtActualizarPasillo->affected_rows <= 0) {
            throw new ExceptionGeneral($mensajeErrorActualizar, $codeErrorActualizar, __FUNCTION__);
        }
        //Confirma los cambios
        $conexion->commit();
    }

    /**
     * Call: Controlador/controladorCajaEstanteriasLibres.php
     * 
     * @global ConexionBDPHP $conexion
     * @return array Estanteria
     * @throws ExceptionGeneral
     * 
     * Obtener las estanterias que tengan huecos libres
     */
    public static function EstanteriasLibres() {
        global $conexion;

        /* Seleccionar las estanterias que tengan lejas libres */
        $stmEstanteriasLibres = "SELECT e.*, p.* FROM estanteria e, pasillo p WHERE lejasOcupadas!=numeroDeLejas AND e.idPasillo=p.idPasillo";
        $resultadoEstanteriasLibres = $conexion->query($stmEstanteriasLibres);
        $num_of_rows = $resultadoEstanteriasLibres->num_rows;

        if ($num_of_rows > 0) {
            while ($row = $resultadoEstanteriasLibres->fetch_object()) {
                $objetoEstanteria = $row;
                $array[] = $objetoEstanteria;
            }
        } else {//Cuando no haya estanterias libres
            $mensajeErrorSelect = "No hay estanterias libres. ";
            $codeErrorSelect = 1002;
            throw new ExceptionGeneral($mensajeErrorSelect, $codeErrorSelect, __FUNCTION__);
        }
        return $array;
    }

    /**
     * Call: Controlador/controladorEstanteriaPasilloNumero.php
     * 
     * @global ConexionBDPHP $conexion
     * @return array Pasillo
     * @throws ExceptionGeneral
     * 
     * Obtener los pasillos que tengan huecos libres para la colocación de estanterías
     */
    public static function EstanteriasPasillosLibres() {

        global $conexion;

        //Seleccionar los pasillos que no esten completos
        $stmEstPasillosLibres = "SELECT * FROM PASILLO WHERE numOcupadas!=(SELECT numeroTotal FROM empresa)";
        $resultadoEstPasillosLibres = $conexion->query($stmEstPasillosLibres);
        $num_of_rows = $resultadoEstPasillosLibres->num_rows;

        if ($num_of_rows > 0) {
            while ($row = $resultadoEstPasillosLibres->fetch_assoc()) {
                //NUMERO TOTAL DE HUECOS DE CADA PASILLO
                $objPasillo = new Pasillo($row['letra']);
                $objPasillo->setIdPasillo($row['idPasillo']);
                $objPasillo->setNumOcupadas($row['numOcupadas']);
                $numeroTotal[] = $objPasillo;
            }
        } else {//Si no hay pasillos libres
            $mensajeErrorSelect = "No quedan pasillos libres. ";
            $codeErrorSelect = 1002;
            throw new ExceptionGeneral($mensajeErrorSelect, $codeErrorSelect, __FUNCTION__);
        }

        return $numeroTotal;
    }

    /**
     * Call: getEstanteriaPasilloNumero.php
     * 
     * @global ConexionBDPHP $conexion
     * @param int $idPasillo
     * @return int numerosRango
     * 
     * Obtener los huecos disponibles en cada pasillo
     */
    public static function EstanteriasNumerosDisponibles($idPasillo) {

        global $conexion;

        $stmtNumeroTotal = "SELECT * FROM empresa";
        $consulta = $conexion->query($stmtNumeroTotal);

        if ($consulta->num_rows > 0) {
            while ($row = $consulta->fetch_assoc()) {
                $filaEmpresa[] = $row;
            }
        }

        //Total de huecos por cada pasillo
        $numeroTotal = $filaEmpresa[0]['numeroTotal'];

        //Array de 1 - numeroTotal
        $numerosRango = range(1, $numeroTotal);

        $stmtNumerosOcupados = $conexion->prepare("SELECT e.* FROM estanteria e, pasillo p WHERE e.idPasillo=p.idPasillo AND p.idPasillo=?");
        $stmtNumerosOcupados->bind_param("i", $idPasillo);
        $stmtNumerosOcupados->execute();
        $resultNumerosOcupados = $stmtNumerosOcupados->get_result();
        $num_of_rows = $resultNumerosOcupados->num_rows;

        //Obtener un array de los números ocupados del pasillo seleccionado
        if ($num_of_rows > 0) {
            while ($row = $resultNumerosOcupados->fetch_row()) {
                $arrayNumerosOcupados[] = $row;
            }
        }

        //Obtener un array con los números disponibles
        for ($j = 0; $j < $num_of_rows; $j++) {
            for ($i = 0; $i < sizeof($numerosRango); $i++) {
                if ($arrayNumerosOcupados[$j][5] == $numerosRango[$i]) { //Si el número es igual al número ocupado se retira del array
                    unset($numerosRango[$i]);
                    $numerosRango = array_values($numerosRango);
                }
            }
        }
        return $numerosRango;
    }

    /**
     * Call: Controlador/getEstanteriaListado.php
     * 
     * @global ConexionBDPHP $conexion
     * @param int $_empezarEn Número del primer registro por el que comenzará el resultado devuelto
     * @param int $_limite Número máximo de filas devueltas por la consulta SELECT
     * @return array Estanterias
     * @throws ExceptionGeneral
     * 
     * Obtener un conjunto de estanterias
     */
    public static function EstanteriasListar($_empezarEn, $_limite) {
        global $conexion;

        //Recuperar registros de una o más tablas en una base de datos y limitar el número de registros devueltos en función de un valor límite
        $stmEstanteriasLibres = "SELECT e.*, p.* FROM estanteria e, pasillo p WHERE e.idPasillo=p.idPasillo ORDER BY p.letra, e.numero ASC LIMIT $_empezarEn,$_limite";
        $resultadoEstanteriasLibres = $conexion->query($stmEstanteriasLibres);

        $num_of_rows = $resultadoEstanteriasLibres->num_rows;

        if ($num_of_rows > 0) {//Si existen registros, crear un array de Estanterias
            while ($row = $resultadoEstanteriasLibres->fetch_object()) {
                $objetoEstanteria = $row;
                $array[] = $objetoEstanteria;
            }
        } else { //Si no existen registros, lanza un error
            $mensajeErrorSelect = "No existen datos. ";
            $codeErrorSelect = 1002;
            throw new ExceptionGeneral($mensajeErrorSelect, $codeErrorSelect, __FUNCTION__);
        }
        return $array;
    }

    /**
     * Call:Controlador/controladorEstanteriaListadoTotalPaginas.php
     * 
     * @global ConexionBDPHP $conexion
     * @return int numero total de estanterias
     * 
     * Obtener la cantidad total de estanterias que hay en la base de datos con 
     * el proposito de crear un listado páginado
     */
    public static function EstanteriaTotalPaginas() {

        global $conexion;

        $stmtTotalPaginas = "SELECT COUNT(idEstanteria) FROM estanteria";
        $resultadoTotPag = $conexion->query($stmtTotalPaginas);
        $row = $resultadoTotPag->fetch_row();
        $totalRecords = $row[0];

        return $totalRecords;
    }

    //CAJAS

    /**
     * Call: Controlador/getCajaLejasLibres.php
     * 
     * @global ConexionBDPHP $conexion
     * @param int $_idEstanteria
     * @return array arrayNumeroDeLejas
     * 
     * Obtener una array de lejas libres de la estanteria seleccionada por el usuario
     */
    public static function CajasLejasLibres($_idEstanteria) {

        global $conexion;

        /* Sacar las lejas que están ocupadas */
        $stmtOcupacion = $conexion->prepare("SELECT lejaOcupada FROM ocupacion WHERE idEstanteriaOcupacion = ?");
        $stmtOcupacion->bind_param("i", $_idEstanteria);
        $stmtOcupacion->execute();
        $resultado = $stmtOcupacion->get_result();
        $num_of_rows = $resultado->num_rows;
        $arrayLejasOcupadas = $resultado->fetch_all(MYSQLI_ASSOC); //Obtener un array asociativo con todos los resultados de la consulta
        //Sacar el numeroTotal de lejas que tiene la estanteria
        $stmtNumeroDeLejas = $conexion->prepare("SELECT numeroDeLejas FROM estanteria WHERE idEstanteria = ?");
        $stmtNumeroDeLejas->bind_param("i", $_idEstanteria);
        $stmtNumeroDeLejas->execute();
        $result = $stmtNumeroDeLejas->get_result();
        $rowEstanteria = $result->fetch_assoc();
        $numeroDeLejasTotal = $rowEstanteria['numeroDeLejas'];

        //Crear array de 1 - numeroDeLejasTotal; Array contiene todas las lejas disponibles de una estanteria.
        $arrayNumeroDeLejas = range(1, $numeroDeLejasTotal);

        //OPCION CON BUCLE FOR
        //Retira las lejas ocupadas del array ($arrayNumerosDeLejas) obteniendo asi las lejas libres    
        /*
          for ($j = 0; $j < $num_of_rows; $j++) {
          for ($i = 0; $i < sizeof($arrayNumeroDeLejas); $i++) {
          if ($arrayLejasOcupadas[$j]['lejaOcupada'] == $arrayNumeroDeLejas[$i]) {
          unset($arrayNumeroDeLejas[$i]);
          $arrayNumeroDeLejas = array_values($arrayNumeroDeLejas);
          }
          }
          } */

        //OPCION CON FOREACH
        //Retira las lejas ocupadas del array obteniendo asi un array con las lejas libres
        foreach ($arrayLejasOcupadas as $lejaOcupada) {
            foreach ($arrayNumeroDeLejas as $indice => $valor) {
                if ($valor == $lejaOcupada['lejaOcupada']) {
                    unset($arrayNumeroDeLejas[$indice]);
                    $arrayNumeroDeLejas = array_values($arrayNumeroDeLejas);
                }
            }
        }
        return $arrayNumeroDeLejas;
    }

    /**
     * Call: Controlador/controladorCajaAlta.php
     * 
     * @global ConexionBDPHP $conexion
     * @param Caja $_objCaja
     * @param Ocupacion $_objOcupacion
     * @throws ExceptionGeneral
     * 
     * Insertar caja en la base de datos, para ello se debe insertar tambien en la tabla de ocupacion para asociar 
     * la caja con la estanteria y la leja que va a ocupar
     */
    public static function CajasInsertar($_objCaja, $_objOcupacion) {

        global $conexion;

        //Variables objeto caja
        $codigoCaja = $_objCaja->getCodigoCaja();
        $color = $_objCaja->getColor();
        $anchura = $_objCaja->getAnchura();
        $altura = $_objCaja->getAltura();
        $profundidad = $_objCaja->getProfundidad();
        $material = $_objCaja->getMaterial();
        $contenido = $_objCaja->getContenido();
        $fechaAlta = $_objCaja->getFechaAlta();

        //Variables objeto ocupacións
        $idEstanteriaOcupacion = $_objOcupacion->getIdEstanteriaOcupacion();
        $lejaOcupada = $_objOcupacion->getLejaOcupada();

        /**
         * Comprobar si el codigo de caja ingresado existe en la tabla backupcaja.
         */
        $stmtSeleccionarBackup = $conexion->prepare("SELECT * FROM backupcaja WHERE codigoCajaBackup = ?");
        $stmtSeleccionarBackup->bind_param("s", $codigoCaja);
        $stmtSeleccionarBackup->execute();
        $result = $stmtSeleccionarBackup->get_result();

        if ($result->num_rows > 0) {
            throw new ExceptionGeneral("Codigo caja existe en backupCaja", "1006", __FUNCTION__);
        }

        /**
         * Insertar en la tabla caja un registro con los datos obtenidos 
         */
        $stmtInsertarCaja = $conexion->prepare("INSERT INTO caja (codigoCaja, color, anchura, altura, profundidad, material, contenido, fechaAlta)"
                . "VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
        $stmtInsertarCaja->bind_param("ssiiisss", $codigoCaja, $color, $anchura, $altura, $profundidad, $material, $contenido, $fechaAlta);
        $stmtInsertarCaja->execute();
        $idCaja = $conexion->insert_id;
        $_objCaja->setIdCaja($idCaja);
        $codeErrorInsertar = $conexion->errno;
        $mensajeErrorInsertar = $conexion->error;

        if ($stmtInsertarCaja->affected_rows <= 0) {
            throw new ExceptionGeneral($mensajeErrorInsertar, $codeErrorInsertar, __FUNCTION__);
        }

        /**
         * Insertar en ocupacion la estanteria y la leja que ocupa la caja
         */
        $stmtInsertarOcupacion = $conexion->prepare("INSERT INTO ocupacion (idCajaOcupacion, idEstanteriaOcupacion, lejaOcupada) "
                . "VALUES (?, ?, ?)");
        $stmtInsertarOcupacion->bind_param("iii", $idCaja, $idEstanteriaOcupacion, $lejaOcupada);
        $stmtInsertarOcupacion->execute();
        $codeErrorInsertarO = $conexion->errno;
        $mensajeErrorInsertarO = $conexion->error;

        if ($stmtInsertarOcupacion->affected_rows <= 0) {
            throw new ExceptionGeneral($mensajeErrorInsertarO, $codeErrorInsertarO, __FUNCTION__);
        }

        /**
         * Actualizar el registro de lejas ocupadas
         */
        $stmtActualizarEstanteria = $conexion->prepare("UPDATE estanteria SET lejasOcupadas = lejasOcupadas + 1 WHERE idEstanteria=?");
        $stmtActualizarEstanteria->bind_param("i", $idEstanteriaOcupacion);
        $stmtActualizarEstanteria->execute();
        $mensajeErrorActualizar = 'No se ha podido realizar la actualización';
        $codeErrorActualizar = '1000';

        if ($stmtActualizarEstanteria->affected_rows <= 0) {
            throw new ExceptionGeneral($mensajeErrorActualizar, $codeErrorActualizar, __FUNCTION__);
        }
    }

    /**
     * 
     * @global ConexionBDPHP $conexion
     * @param String $_key
     * @return array
     * 
     * Obtiene un conjunto de registros de la tabla caja cuyo coincida con los caracteres introducidos
     */
    public static function sugerenciaCaja($_key) {
        global $conexion;

        $stmt = 'SELECT * FROM caja c WHERE c.codigoCaja LIKE "%' . $_key . '%"';
        $res = $conexion->query($stmt);

        if ($res->num_rows > 0) {
            while ($row = $res->fetch_object()) {

                $array[] = $row;
            }
        } else {
            $array = array();
        }

        return $array;
    }

    public static function CajasObtenerPorCodigo($_codigo) {

        global $conexion;

        $stmtObtenerCaja = $conexion->prepare("SELECT DISTINCT c.*, oc.*, e.*, p.* FROM caja c, ocupacion oc, estanteria e, pasillo p WHERE c.idCaja IN 
                (SELECT DISTINCT o.idCajaOcupacion FROM ocupacion o WHERE o.idCajaOcupacion =
                (SELECT DISTINCT idCaja FROM caja WHERE codigoCaja = ?)
                ) AND c.idCaja=oc.idCajaOcupacion AND oc.idEstanteriaOcupacion=e.idEstanteria AND e.idPasillo=p.idPasillo;");

        $stmtObtenerCaja->bind_param("s", $_codigo);
        $stmtObtenerCaja->execute();
        $resObtenerCaja = $stmtObtenerCaja->get_result();

        if ($resObtenerCaja->num_rows > 0) {
            while ($row = $resObtenerCaja->fetch_object()) {
                $array[] = $row;
            }
        }
        return $array;
    }

    /**
     * Call: Controlador/controladorCajaVenta.php
     * 
     * @global ConexionBDPHP $conexion
     * @param type $_codigo
     * @throws ExceptionGeneral
     * 
     * Vender caja. Borrar caja de tabla caja
     */
    public static function CajasVenta($_codigo) {

        global $conexion;

        $stmtVentaCaja = $conexion->prepare("DELETE FROM caja WHERE codigoCaja = ?");
        $stmtVentaCaja->bind_param("s", $_codigo);
        $stmtVentaCaja->execute();

        if ($stmtVentaCaja->affected_rows === 0) {
            throw new ExceptionGeneral("No se ha podido efectuar la venta", "1005", __FUNCTION__);
        }
    }

    public static function CajaDevolucion($_key) {
        global $conexion;

        $stmt = 'SELECT * FROM backupcaja bc WHERE bc.codigoCajaBackup LIKE "%' . $_key . '%"';
        $res = $conexion->query($stmt);

        if ($res->num_rows > 0) {
            while ($row = $res->fetch_assoc()) {

                $codigoCaja = $row['codigoCajaBackup'];
                $color = $row['color'];
                $anchura = $row['anchura'];
                $altura = $row['altura'];
                $profundidad = $row['profundidad'];
                $material = $row['material'];
                $contenido = $row['contenido'];
                $fechaAlta = $row['fechaAlta'];
                $fechaVenta = $row['fechaVenta'];
                $lejaOcupada = $row['lejaOcupada'];
                $codigoEstanteriaBackup = $row['codigoEstanteriaBackup'];


                $objetoBackupCaja = new BackupCaja($codigoCaja, $color, $anchura, $altura, $profundidad, $material, $contenido, $fechaAlta, $fechaVenta, $lejaOcupada, $codigoEstanteriaBackup);
                $array[] = $objetoBackupCaja;
            }//CAmbiar a objeto caja
        } else {
            $array = array();
        }

        return $array;
    }

    public static function CajasBackupObtenerPorCodigo($_codigo) {
        global $conexion;

        $stmt = 'SELECT * FROM backupcaja bc WHERE bc.codigoCajaBackup LIKE "%' . $_codigo . '%"';
        $res = $conexion->query($stmt);

        if ($res->num_rows > 0) {
            while ($row = $res->fetch_object()) {
                $array[] = $row;
            }
        } else {
            $array = array();
        }

        return $array;
    }

    /**
     * Call: Controlador/controladorCajaListadoTotalPaginas.php
     * 
     * @global ConexionBDPHP $conexion
     * @return int numero total de cajas
     * 
     * Retorna el numero total de cajas
     */
    public static function CajasTotalPaginas() {

        global $conexion;

        //Obtener el número total de cajas de la tabla caja
        $stmtTotalPaginas = "SELECT COUNT(idCaja) FROM caja";
        $resultadoTotPag = $conexion->query($stmtTotalPaginas);
        $row = $resultadoTotPag->fetch_row();
        $totalRecords = $row[0];

        return $totalRecords;
    }

    /**
     * Call: Controlador/getCajaListado.php
     * 
     * @global ConexionBDPHP $conexion
     * @param type $_empezarEn Número del primer registro por el que comenzarça el resultado devuelto
     * @param type $_limite Número máximo de filas devueltas por la consulta SELECT
     * @return array Cajas
     * @throws ExceptionGeneral
     */
    public static function CajasListar($_empezarEn, $_limite) {
        global $conexion;

        $stmtCajas = "SELECT * FROM caja ORDER BY fechaAlta ASC LIMIT $_empezarEn,$_limite";
        $resultadoCajas = $conexion->query($stmtCajas);

        $num_of_rows = $resultadoCajas->num_rows;

        if ($num_of_rows > 0) {
            while ($row = $resultadoCajas->fetch_object()) {
                $objetoCaja = $row;
                $array[] = $objetoCaja;
            }
        } else {
            $mensajeErrorSelect = "No existen datos. ";
            $codeErrorSelect = 1002;
            throw new ExceptionGeneral($mensajeErrorSelect, $codeErrorSelect, __FUNCTION__);
        } //Cuando no haya estanterias libres
        return $array;
    }

    /**
     * Call: Controlador/controladorCajaExistenDatos.php
     * 
     * @global ConexionBDPHP $conexion
     * @return boolean
     * @throws ExceptionGeneral
     * 
     * Comprobar si existen cajas
     */
    public static function CajasExisten() {
        global $conexion;

        $stmtCajas = "SELECT * FROM caja";
        $resultadoCajas = $conexion->query($stmtCajas);

        $num_of_rows = $resultadoCajas->num_rows;

        if ($num_of_rows > 0) {
            return true;
        } else {
            $mensajeErrorSelect = "No existen datos";
            $codeErrorSelect = 1002;
            throw new ExceptionGeneral($mensajeErrorSelect, $codeErrorSelect, __FUNCTION__);
        } //Cuando no haya estanterias libres
    }

    /**
     * Call: Controlador/controladorCajaExistenDatos.php
     * 
     * @global ConexionBDPHP $conexion
     * @return boolean
     * @throws ExceptionGeneral
     * 
     * Comprobar si existen cajas en backup
     */
    public static function CajasExistenBackup() {
        global $conexion;

        $stmtCajas = "SELECT * FROM backupcaja";
        $resultadoCajas = $conexion->query($stmtCajas);

        $num_of_rows = $resultadoCajas->num_rows;

        if ($num_of_rows > 0) {
            return true;
        } else {
            $mensajeErrorSelect = "No existen datos";
            $codeErrorSelect = 1002;
            throw new ExceptionGeneral($mensajeErrorSelect, $codeErrorSelect, __FUNCTION__);
        }
    }

    //GESTIÓN

    /**
     * Call: Controlador/controladorGestionInventario.php
     * 
     * @global ConexionBDPHP $conexion
     * @return objeto inventario
     * @throws ExceptionGeneral
     * 
     * Obtener inventario
     */
    public static function gestionInventario() {

        global $conexion;

        /*
         * Consulta para obtener cada estanteria con las cajas que tiene
         */
        $stmtInventario = "SELECT DISTINCT estanteria.*, estanteria.material AS estanteria_material, caja.material AS caja_material, caja.*,pasillo.letra, ocupacion.lejaOcupada FROM estanteria
                            LEFT JOIN ocupacion ON estanteria.idEstanteria=ocupacion.idEstanteriaOcupacion
                            LEFT JOIN caja ON caja.idCaja = ocupacion.idCajaOcupacion
                            LEFT JOIN pasillo ON estanteria.idPasillo=pasillo.idPasillo
                            ORDER BY estanteria.numero, pasillo.letra, ocupacion.lejaOcupada;";
        $resultadoStmtInventario = $conexion->query($stmtInventario);
        $num_of_rows = $resultadoStmtInventario->num_rows;
        $mensajeErrorInventario = "No existen datos";
        $codeErrorInventario = "1004";

        if ($num_of_rows > 0) {
            while ($rowInventario = $resultadoStmtInventario->fetch_object()) {
                $arrayInventario[] = $rowInventario;
            }
        } else {
            throw new ExceptionGeneral($mensajeErrorInventario, $codeErrorInventario, __FUNCTION__);
        }
        return $arrayInventario;
    }

    /**
     * Call: Controlador/controladorCajaDevolucion.php
     * 
     * @global ConexionBDPHP $conexion
     * @param type $_objCaja
     * @param type $_objOcupacion
     * @throws ExceptionGeneral
     * 
     * Gestionar la devolucion de caja
     */
    public static function gestionDevolucion($_objCaja, $_objOcupacion) {
        global $conexion;

        $codigoCaja = $_objCaja->getCodigoCaja();

        $idEstanteria = $_objOcupacion->getIdEstanteriaOcupacion();
        $lejaOcupada = $_objOcupacion->getLejaOcupada();

        include_once '../Modelo/triggerDevolucionCaja.php';

        $stmtDevolucion = $conexion->prepare("DELETE FROM backupcaja WHERE codigoCajaBackup = ?");
        $cod = '1004';
        $stmtDevolucion->bind_param("s", $codigoCaja);
        $stmtDevolucion->execute();

        /*
          $sqltb = ("DROP TRIGGER IF EXISTS devolucionCaja");
          $resul = $conexion->query($sqltb);
          $codeErrorDrop = $conexion->errno;
         * 
         */

        if ($stmtDevolucion->affected_rows == 0) {
            throw new ExceptionGeneral("No se ha podido realizar la devolución", "1007", __FUNCTION__);
        } else if (!$res) {
            throw new ExceptionGeneral("No se ha podido realizar la devolución", "1007", __FUNCTION__);
        }
    }

    //AUTENTIFICACIÓN

    /**
     * Call: Controlador/controlAutentificacionInicio.php
     * 
     * @global ConexionBDPHP $conexion
     * @return boolean
     * Retorna true si se encuentran usuarios en la tabla Usuario
     * Retorna false si no se encuentran usuarios
     * 
     * Comprueba si existen usuarios en la base de datos
     */
    public static function inicio() {

        global $conexion;

        $stmtInicio = "SELECT * FROM usuario";
        $resultado = $conexion->query($stmtInicio);

        if ($resultado->num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Call: Controlador/controladorAutenticacionRegistro.php
     * 
     * @global ConexionBDPHP $conexion
     * @param Usuario $_objUsuario
     * @throws ExceptionGeneral
     * 
     * Insertar un usuario
     */
    public static function registro($_objUsuario) {
        global $conexion;

        //Variables objeto usuario
        $usuario = $_objUsuario->getUsuario();
        $nombreUsuario = $_objUsuario->getNombreUsuario();
        $apellidoUsuario = $_objUsuario->getApellidoUsuario();
        $passUsuario = $_objUsuario->getPassUsuario();

        //Encriptar contraseña 
        $passUsuarioCodificada = password_hash($passUsuario, PASSWORD_BCRYPT);

        //Consulta inserccion Usuario
        $stmtRegistroUsuario = $conexion->prepare("INSERT INTO usuario (usuario, nombreUsuario, apellidoUsuario, passUsuario) VALUES (?, ?, ?, ?)");
        $stmtRegistroUsuario->bind_param("ssss", $usuario, $nombreUsuario, $apellidoUsuario, $passUsuarioCodificada);
        $stmtRegistroUsuario->execute();
        $codeErrorRegistroUsuario = $conexion->errno;
        $mensajeErrorRegistroUsuario = $conexion->error;

        //Si no se ha podido realizar la insercción de usuario
        if ($stmtRegistroUsuario->affected_rows <= 0) {
            throw new ExceptionGeneral($mensajeErrorRegistroUsuario, $codeErrorRegistroUsuario, __FUNCTION__);
        }
    }

    /**
     * Call: Controlador/controlAutentificacionIngreso.php
     * 
     * @global ConexionBDPHP $conexion
     * @param Usuario $_objUsuario
     * @return boolean
     * 
     * Retorna true si la contraseña y usuario coincide con el registro de la base de datos
     * Retorna false si no hay ninguna coincidencia con los registros de la base de datos
     * 
     * Comprobar si la contraseña y usuario existen en la base de datos
     */
    public static function ingreso($_objUsuario) {
        global $conexion;

        //Variables objeto usuarios
        $usuario = $_objUsuario->getUsuario();
        $passUsuario = $_objUsuario->getPassUsuario();

        //Consulta registros Usuario
        $stmtIngresoUsuario = "SELECT * FROM usuario";
        $resIngregoUsuario = $conexion->query($stmtIngresoUsuario);
        $fila = $resIngregoUsuario->fetch_assoc();

        if ($fila) { //Si existe un registro en la tabla Usuario, comprueba si la contraseña y el usuario coinciden con los datos introducimos por el usuario
            if (password_verify($passUsuario, $fila['passUsuario']) && $fila['usuario'] == $usuario) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //CONTENEDORES

    public static function contenedoresLibres() {
        global $conexion;

        /* Seleccionar las estanterias que tengan lejas libres */
        $stmEstanteriasLibres = "SELECT * FROM contenedor WHERE numOcupados != capacidadContenedor;";
        $resultadoEstanteriasLibres = $conexion->query($stmEstanteriasLibres);
        $num_of_rows = $resultadoEstanteriasLibres->num_rows;

        if ($num_of_rows > 0) {
            while ($row = $resultadoEstanteriasLibres->fetch_object()) {
                $objetoEstanteria = $row;
                $array[] = $objetoEstanteria;
            }
        } else {//Cuando no haya estanterias libres
            $mensajeErrorSelect = "No hay contenedores libres. ";
            $codeErrorSelect = 1002;
            throw new ExceptionGeneral($mensajeErrorSelect, $codeErrorSelect, __FUNCTION__);
        }
        return $array;
    }

    public static function huecosContenedor($idContenedor) {

        global $conexion;

        //SELECT capacidadContenedor-numOcupados FROM contenedor WHERE idContenedor = 1

        $stmtContenedor = $conexion->prepare("SELECT capacidadContenedor - numOcupados AS resta FROM contenedor WHERE idContenedor = ?");
        $stmtContenedor->bind_param("i", $idContenedor);
        $stmtContenedor->execute();
        $resultado = $stmtContenedor->get_result();
        $num_of_rows = $resultado->num_rows;



        if ($num_of_rows > 0) {
            while ($row = $resultado->fetch_assoc()) {
                $huecos = $row['resta'];
            }
        } else {//Cuando no haya estanterias libres
            $mensajeErrorSelect = "No hay contenedores libres. ";
            $codeErrorSelect = 1002;
            throw new ExceptionGeneral($mensajeErrorSelect, $codeErrorSelect, __FUNCTION__);
        }
        return $huecos;
    }

    public static function CajasVentaConContenedor($_codigo, $_idContenedor, $idEstanteria) {

        global $conexion;



        include_once '../Modelo/triggerVentaCaja.php';

        $stmtVentaCaja = $conexion->prepare("DELETE FROM ocupacion WHERE idCajaOcupacion = (SELECT idCaja FROM caja WHERE codigoCaja=?)");
        $stmtVentaCaja->bind_param("s", $_codigo);
        $stmtVentaCaja->execute();

        if ($stmtVentaCaja->affected_rows === 0) {
            throw new ExceptionGeneral("No se ha podido efectuar la venta", "1005", __FUNCTION__);
        }
    }

    public static function listadoContendor($codigoContenedor) {

        global $conexion;

        /*
         * Consulta para obtener cada estanteria con las cajas que tiene
         */
        $stmtInventario = "SELECT caja.* FROM caja 
WHERE idCaja = (SELECT idCajaOcupacionContenedor FROM ocupacionContenedor
WHERE idOcupacionContenedor=(SELECT idContenedor FROM contenedor WHERE
codigoContenedor='" . $codigoContenedor . "'))";
        $resultadoStmtInventario = $conexion->query($stmtInventario);
        $num_of_rows = $resultadoStmtInventario->num_rows;
        $mensajeErrorInventario = "No existen datos";
        $codeErrorInventario = "1004";

        if ($num_of_rows > 0) {
            while ($rowInventario = $resultadoStmtInventario->fetch_object()) {
                $arrayInventario[] = $rowInventario;
            }
        } else {
            throw new ExceptionGeneral($mensajeErrorInventario, $codeErrorInventario, __FUNCTION__);
        }
        return $arrayInventario;
    }

}
