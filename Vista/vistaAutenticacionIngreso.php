<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="../Estilos/style.css">
    <style>
        #titulo{
            color:black !important;
        }
        .form-group{
            margin-bottom:0.6rem;
        }

        #seccionContainer .container{
            width: 50% !important;
            border: 2px solid black;
            box-shadow: 2px 2px 5px #000;
        }

        form{
            width: auto !important;
            text-align:center;
        }
        
        body{
            background-image: url('../Estilos/img/fondoLogin.jpg');
            background-size: cover;
        }


    </style>
    <body>
        <section id="seccionContainer" >
            <div class="container rounded mt-5 opaco">
                <form action="../Controlador/controlAutentificacionIngreso.php" >
                    <h1 id="titulo" class="text-center">Login</h1>       
                    <!--USUARIO-->
                    <div class="form-group row">
                        <label for="inputUsuario" class="col-sm-2 col-form-label" >Usuario</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control " id="inputUsuario" name="Usuario" placeholder="Usuario" maxlength="25" minlength="1" required>
                        </div>
                    </div>
                    <!--PASS-->
                    <div class="form-group row">
                        <label for="inputPassUsuario" class="col-sm-2 col-form-label" >Password</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control " id="inputPassUsuario" name="PassUsuario" placeholder="Password" maxlength="25" minlength="1" required>
                        </div>
                    </div>
                    <input class="btn btn-outline-primary " type="submit" value="Login">
                </form>

            </div>
        </section>
    </body>
</html>
