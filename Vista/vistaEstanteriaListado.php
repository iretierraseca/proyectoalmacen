<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php include_once 'menu.php'; ?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <!--<script src="../JavaScript/ajaxPagination.js"></script>-->

   <!-- <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.0.3.js"></script>-->
    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
    <script src="../JavaScript/jquery.simplePagination.js"></script>
    <style>
        #seccionContainer .container{
            width: 50% !important;
            border: 2px solid #daae87;
            box-shadow: 2px 2px 5px #daae87;
        }

    </style>

    <body >
        <?php
        $totalRecords = $_REQUEST['totalRecords'];
        //Limite de registros por cada página
        $limite = 6;
        ?>
        <section id="seccionContainer" >
            <div class="container shadow  rounded opaco">
                <div id="target-content"><h4 class="text-center">LISTADO ESTANTERIAS</h4></div>

                <nav aria-label="Page navigation example ">

                    <ul class="pagination justify-content-center" id="pagination">

                    </ul>
                </nav>
                <table class="table table-bordered table-striped" id="tabla">

                </table>
            </div>
        </section>
        <script type="text/javascript">
            $(document).ready(function () {
                var limite = <?php echo $limite ?>;
                $("#tabla").load("../Controlador/getEstanteriaListado.php", {
                    pagina: 1,
                    limite: limite
                }, function (response, status, xhr) {
                    if (status == "error") {
                        var msg = "Error!, algo ha sucedido: ";
                        $("#tabla").html(response);
                    }
                });
                $('.pagination').pagination({
                    items: <?php echo $totalRecords; ?>,
                    itemsOnPage: <?php echo $limite; ?>,
                    cssStyle: 'dark-theme',
                    currentPage: 1,
                    onPageClick: function (pageNumber) {
                        $("#tabla").html('loading...');
                        $("#tabla").load("../Controlador/getEstanteriaListado.php", {
                            pagina: pageNumber,
                            limite: limite
                        }, function (response, status, xhr) {

                            if (status == "error") {
                                var msg = "Error!, algo ha sucedido: ";
                                $("#tabla").html(response);
                            }
                        });
                    }
                });
            });
        </script>
    </body>
</html>
