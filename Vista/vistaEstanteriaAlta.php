<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
// put your code here
include_once '../Modelo/Pasillo.php';
session_start();
include_once 'menu.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            #titulo{
                color:black !important;
            }

            #seccionContainer .container{
                width: 50% !important;
                border: 2px solid #daae87;
                box-shadow: 2px 2px 5px #daae87;
            }

            form{
                width: auto !important;
                text-align:center;
            }

        </style>
    </head>
    <body>
        <section id="seccionContainer" >
            <div class="container shadow  rounded opaco">
                <form action="../Controlador/controladorEstanteriaAlta.php">
                    <h1 id="titulo">Estanteria</h1>
                    <!--CODIGO-->
                    <div class="form-group row">
                        <label for="inputCodigoEstanteria" class="col-sm-2 col-form-label" >Código</label>
                        <div class="col-sm-10">
                            <input type="text" pattern="[E-E][S-S][0-9][0-9][0-9]"class="form-control " id="inputCodigoEstanteria" name="CodigoEstanteria" placeholder="Código" maxlength="5" minlength="5" required value="ES" >
                        </div>
                    </div>
                    <!--MATERIAL-->
                    <div class="form-group row">
                        <label for="inputMaterialEstanteria" class="col-sm-2 col-form-label">Material</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputMaterialEstanteria" name="MaterialEstanteria" placeholder="Material" required>
                        </div>
                    </div>
                    <!--NUMERO LEJAS-->
                    <div class="form-group row">
                        <label for="inputNumLejasEstanteria" class="col-sm-2 col-form-label">Num lejas</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" id="inputNumLejasEstanteria" name="NumLejasEstanteria" placeholder="Número de lejas" required min="0">
                        </div>
                    </div>
                    <!-- PASILLO Y NÚMERO -->
                    <?php
                    $arrayObjPasilloLibres = $_SESSION['PasillosLibres'];
                    ?>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputPasilloEstanteria">Pasillo</label>
                            
                            <select class="form-control " name="selectPasilloEstanteria" id="selectPasilloEstanteria" onchange="muestraNumerosEstanteria(this.value)">
                                <option value="null">Elige un pasillo</option>
                                <?php foreach ($arrayObjPasilloLibres as $objPasillo) { ?>
                                    <option value="<?php echo $objPasillo->getIdPasillo() ?>">
                                        <?php
                                        $letraPasillo = $objPasillo->getLetra();
                                        echo 'Pasillo: ' . $letraPasillo;
                                        ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="ListaNumeroEstanteria">Número</label>

                            <select class="form-control" name="ListaNumeroEstanteria" id="ListaNumeroEstanteria">

                                
                            </select>
                        </div>
                        <script type='text/javascript' src='../JavaScript/ajaxNumeroPasillo.js'></script>
                    </div>
                    <input class="btn btn-outline-primary" type="submit" id="botonEnviarEstanteriaCaja" disabled>
                </form>
            </div>
        </section>  
        <script>
            $('#inputCodigoEstanteria').keyup(function (e) {
                if (this.value.length < 2) {
                    this.value = 'ES';
                } else if (this.value.indexOf('ES') !== 0) {
                    this.value = 'ES' + String.fromCharCode(e.which);
                }
            });


        </script>
    </body>
</html>
