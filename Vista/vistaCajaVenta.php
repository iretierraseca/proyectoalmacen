<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
session_start();
include_once 'menu.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <style>
        #titulo{
            color:black !important;
        }
        .form-group{
            margin-bottom:0.6rem;
        }

        #seccionContainer .container{
            width: 50% !important;
            border: 2px solid #a98257;
            box-shadow: 2px 2px 5px #a98257;
        }

        form{
            width: auto !important;
            text-align:center;
        }
        #suggestions{
            box-shadow: 2px 2px 8px 0 rgba(0,0,0,.2);
            max-height: 200px;
            position: absolute;
            top: 45px;
            z-index: 9999;
            width: 206px;
            overflow-y:auto;
        }

        #suggestions .suggest-element {
            background-color: #EEEEEE;
            border-top: 1px solid #d6d4d4;
            cursor: pointer;
            padding: 8px;
            width: 100%;
            float: left;

        }


    </style>
    <body>
        <?php include_once '../Modelo/Caja.php'; ?>
        <?php include_once '../Modelo/Contenedor.php'; ?>
        <section id="seccionContainer" >
            <div class="container rounded mt-5 opaco" >
                <form action="../Controlador/controladorCajaVenta.php" autocomplete="off">
                    <h1 id="titulo" class="text-center">Venta caja</h1>
                    <!--CODIGO-->
                    <div class="form-group row">
                        <label for="inputCodigoCaja" class="col-sm-2 col-form-label" >Código</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control " id="inputCodigoCaja" name="CodigoCaja" placeholder="Código" maxlength="5" minlength="5" required value="CA">
                            <div id="suggestions">
                            </div>
                        </div>

                    </div>

                    <!--Color-->
                    <div class="form-group row">
                        <label for="inputColorCaja" class="col-sm-2 col-form-label">Color</label>
                        <div class="col-sm-10">
                            <input disabled type="color" class="form-control " id="inputColorCaja" name="ColorCaja" required>
                        </div>
                    </div>
                    <!-- ANCHURA, ALTURA, PROFUNDIDAD -->
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputAnchuraCaja">Anchura</label>
                            <input readonly type="number" class="form-control " id="inputAnchuraCaja" name="AnchuraCaja" placeholder="Anchura" required min="0">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputAlturaCaja">Altura</label>
                            <input readonly type="number" class="form-control " id="inputAlturaCaja" name="AlturaCaja" placeholder="Altura" required min="0">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputProfundidadCaja">Profundidad</label>
                            <input readonly type="number" class="form-control " id="inputProfundidadCaja" name="ProfundidadCaja" placeholder="Profundidad" required min="0">
                        </div>
                    </div>
                    <!--MATERIAL-->
                    <div class="form-group row">
                        <label for="inputMaterialCaja" class="col-sm-2 col-form-label">Material</label>
                        <div class="col-sm-10">
                            <input readonly type="text" class="form-control " id="inputMaterialCaja" name="MaterialCaja"  placeholder="Material" required>
                        </div>
                    </div>
                    <!--CONTENIDO-->
                    <div class="form-group row">
                        <label for="inputContenidoCaja" class="col-sm-2 col-form-label">Contenido</label>
                        <div class="col-sm-10">
                            <input readonly type="text" class="form-control" id="inputContenidoCaja" name="ContenidoCaja" placeholder="Contenido" required>
                        </div>
                    </div>
                    <!-- ESTANTERIA, LEJA -->
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputAnchuraCaja">Estanteria</label>
                            <input readonly type="text" class="form-control" placeholder="Estanteria" id="inputEstanteriaCaja" name="inputEstanteriaCaja">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputLejaCaja">Leja</label>
                            <input readonly type="text" class="form-control" placeholder="Leja" id="inputLejaCaja">
                        </div>
                    </div>
                    <!--CONTENIDO-->
                    <div class="form-group row">
                        <label for="inputFechaCaja" class="col-sm-2 col-form-label">Fecha</label>
                        <div class="col-sm-10">
                            <input readonly type="date" class="form-control " id="inputFechaCaja" name="FechaCaja" placeholder="Contenido" required>
                        </div>
                    </div>
                    <?php
                    $arrayObjEstanteriasLibres = $_SESSION['contenedoresLibres'];
                    ?>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputAnchuraCaja">Contenedor</label>

                            <select class="form-control " name="selectInfoControlador" id="selectInfoContenedor" onchange="muestraContenedoresLibres(this.value)" required>
                                <option value="null">Elige un contenedor</option>
                                <?php foreach ($arrayObjEstanteriasLibres as $objContenedor) { ?>
                                    <option value="<?php echo $objContenedor->idContenedor?>">
                                        <?php
                                        $codigoContenedor = $objContenedor->codigoContenedor;
                                        echo 'Contenedor: ' . $codigoContenedor;
                                        ?></option>
                                <?php } ?>
                            </select>

                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputCapacidadContenedor">Capacidad</label>
                            
                            <select class="form-control " name="inputCapacidadContenedor" id="inputCapacidadContenedor">
                                <option>Hueco</option>
                            </select>

                        </div>
                        <script src="../JavaScript/ajaxContenedoresLibres.js"></script>
                    </div>

                    <input class="btn btn-outline-primary " type="submit" id="botonEnviarVentaCaja" disabled>

                </form>


            </div>
        </section>  
        <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
        <script>
                                $(document).ready(function () {

                                    $('#inputCodigoCaja').on('keyup', function () {
                                        var key = $(this).val();
                                        //alert($(this).val().length);
                                        if ($(this).val().length < 5) {
                                            $('input[type=submit]').attr("disabled", "");
                                        }
                                        if (this.value.length < 2) {
                                            this.value = 'CA';
                                        }
                                        if (key != "CA") { //Si no hay escrito nada en el campo de texto que no se muestren las sugerencias
                                            $.ajax({

                                                type: "POST",
                                                url: "../Controlador/getCajaVenta.php",
                                                data: {
                                                    key: key,
                                                    inputCodigoCaja: 'autocompletado'
                                                },
                                                success: function (data) {
                                                    //Escribimos las sugerencias que nos manda la consulta
                                                    $('#suggestions').fadeIn(1000).html(data);
                                                    //Al hacer click en alguna de las sugerencias
                                                    $('.suggest-element').on('click', function () {
                                                        var valor = $(this).attr('data');
                                                        if (valor != 'No hay datos') {
                                                            $('#inputCodigoCaja').val(valor);
                                                        } else {
                                                            $('#inputCodigoCaja').val("CA");
                                                        }
                                                        $('#suggestions').fadeOut(500);

                                                        $.ajax({
                                                            type: "POST",
                                                            url: "../Controlador/getCajaVenta.php",
                                                            data: {valor: valor},
                                                            dataType: "JSON",
                                                            success: function (data) {
                                                                $("#inputColorCaja").val(data[0]['color']);
                                                                $("#inputAnchuraCaja").val(data[0]['anchura']);
                                                                $("#inputAlturaCaja").val(data[0]['altura']);
                                                                $("#inputProfundidadCaja").val(data[0]['profundidad']);
                                                                $("#inputMaterialCaja").val(data[0]['material']);
                                                                $("#inputContenidoCaja").val(data[0]['contenido']);
                                                                $("#inputFechaCaja").val(data[0]['fechaAlta']);
                                                                var estanteria = "Codigo:" + data[0]['codigoEstanteria'] + " pasillo:" + data[0]['letra'] + " número:" + data[0]['numero'];
                                                                $('#inputEstanteriaCaja').val(data[0]['idEstanteria']);
                                
                                                                $('#inputLejaCaja').val(data[0]['lejaOcupada']);

                                                                $('input[type=submit]').removeAttr("disabled");
                                                            }
                                                        });
                                                    }
                                                    );
                                                }
                                            });
                                        }
                                        if ($(this).val() == "CA") { //Cuando se borra el texto que desaparezcan las sugerencias
                                            $('#suggestions').fadeOut(500);
                                        }


                                    });
                                });
        </script>
    </body>
</html>
