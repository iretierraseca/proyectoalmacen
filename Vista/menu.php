<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="../Estilos/style.css">
    </head>
    <body>
        <div class="contenedor">

            <!-- START NAV -->
            <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
                <div class="container">
                    <a class="navbar-brand" href="#" id="enlace">
                        <h1 class="d-inline-block align-top" id="icono">KUTXAL</h1>
                    </a>
                    <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03"
                            aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                        <ul class="navbar-nav ml-auto mt-2 mt-lg-0 ">
                            <li class="nav-item active p-1">
                                <a class="nav-link link-ejercicios" href="inicio.php" id="ejercicio1.1">Inicio</a>
                            </li>
                            <!--ESTANTERIA-->
                            <li class="nav-item dropdown p-1">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">
                                    Estanteria
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="../Controlador/controladorEstanteriaPasilloNumero.php">Alta</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="../Controlador/controladorEstanteriaListadoTotalPaginas.php">Listado</a>
                                </div>
                            </li>
                            <!--CAJAS-->
                            <li class="nav-item dropdown p-1">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">
                                    Cajas
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="../Controlador/controladorCajaEstanteriasLibres.php?opcionMenu=AltaCaja">Alta</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="../Controlador/controladorCajaListadoTotalPaginas.php">Listado</a>
                                    <a class="dropdown-item" href="../Vista/vistaContenedor.php">Listado Contenedor</a>
                                </div>
                            </li>
                            <!--GESTIÓN-->
                            <li class="nav-item dropdown p-1">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">
                                    Gestión
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="../Controlador/controladorGestionInventario.php">Inventario</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="../Controlador/controladorCajaExistenDatos.php?opcionLlamada=VentaCaja">Venta</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="../Controlador/controladorCajaEstanteriasLibres.php?opcionMenu=DevolucionCaja">Devolución</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- END NAV -->

            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>
    </body>
</html>
