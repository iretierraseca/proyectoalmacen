<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
// put your code here
session_start();
include_once 'menu.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            #inventario{
                height: 80%;
                overflow-y: scroll;
            }

            #filaDatosEst{
                background-color:#a98257 !important;
            }

            #seccionContainer .container{
                border: 2px solid #f5d5be;
                box-shadow: 2px 2px 5px #f5d5be;
            }
            
        </style>
    </head>
    <body>
        <?php
        $inventario = $_SESSION['inventario'];
        ?>
        <section id="seccionContainer" >
            <div class="container rounded mt-5 opaco" id="inventario">
                <h3 class="text-center">Inventario</h3>
                <table border="1" width="100%" class="mt-2 table table-bordered text-center">


                    <?php
                    $auxiliar = "";
                    foreach ($inventario as $objetoInventario) {
                        $codigoEst = $objetoInventario->codigoEstanteria;

                        if ($auxiliar !== $codigoEst) {
                            ?>
                            <thead>
                                <tr>
                                    <th colspan="2">Codigo</th>
                                    <th colspan="2">Material</th>
                                    <th colspan="2">Numero lejas</th>
                                    <th>Pasillo</th>
                                    <th>Número</th>
                                    <th>Lejas ocupadas</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="filaDatosEst">
                                    <td colspan="2"><?php echo $objetoInventario->codigoEstanteria ?></td>
                                    <td colspan="2"><?php echo $objetoInventario->estanteria_material ?></td>
                                    <td colspan="2"><?php echo $objetoInventario->numeroDeLejas ?></td>
                                    <td><?php echo $objetoInventario->letra ?></td>
                                    <td><?php echo $objetoInventario->numero ?></td>
                                    <td><?php echo $objetoInventario->lejasOcupadas ?></td>
                                </tr>


                                <?php
                            }
                            $auxiliar = $codigoEst;
                            ?>

                            <?php if ($objetoInventario->codigoCaja != "") { ?>
                                <tr>
                                    <th>Codigo</th>
                                    <th>color</th>
                                    <th>anchura</th>
                                    <th>altura</th>
                                    <th>profundidad</th>
                                    <th>material</th>
                                    <th>contenido</th>
                                    <th>Leja ocupada</th>
                                    <th>fecha de alta</th>
                                </tr>
                                <tr>
                                    <td><?php echo $objetoInventario->codigoCaja ?></td>
                                    <td><input type="color" value="<?php echo $objetoInventario->color ?>" disabled></td>
                                    <td><?php echo $objetoInventario->anchura ?></td>
                                    <td><?php echo $objetoInventario->altura ?></td>
                                    <td><?php echo $objetoInventario->profundidad ?></td>
                                    <td><?php echo $objetoInventario->caja_material ?></td>
                                    <td><?php echo $objetoInventario->contenido ?></td>
                                    <td><?php echo $objetoInventario->lejaOcupada ?></td>
                                    <td><?php echo $objetoInventario->fechaAlta ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                        <?php
                    }
                    ?>
                </table>

            </div>
        </section>
    </body>
</html>
