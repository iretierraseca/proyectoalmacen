<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
// put your code here
include_once '../Modelo/Estanteria.php';
session_start();
include_once 'menu.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            #titulo{
                color:black !important;
            }
            .form-group{
                margin-bottom:0.6rem;
            }

            #seccionContainer .container{
                width: 50% !important;
                border: 2px solid #a98257;
                box-shadow: 2px 2px 5px #a98257;
            }

            form{
                width: auto !important;
                text-align:center;
            }



        </style>

    </head>
    <body>
        <section id="seccionContainer" >
            <div class="container rounded mt-5 opaco">
                <form action="../Controlador/controladorCajaAlta.php">
                    <h1 id="titulo" class="text-center">Caja</h1>
                    <!--CODIGO-->
                    <div class="form-group row">
                        <label for="inputCodigoCaja" class="col-sm-2 col-form-label" >Código</label>
                        <div class="col-sm-10">
                            <input type="text"  pattern="[C-C][A-A][0-9][0-9][0-9]" class="form-control " id="inputCodigoCaja" name="CodigoCaja" placeholder="Código" maxlength="5" minlength="5" required value="CA">
                        </div>
                    </div>
                    <!--Color-->
                    <div class="form-group row">
                        <label for="inputColorCaja" class="col-sm-2 col-form-label">Color</label>
                        <div class="col-sm-10">
                            <input type="color" class="form-control " id="inputColorCaja" name="ColorCaja" required>
                        </div>
                    </div>
                    <!-- ANCHURA, ALTURA, PROFUNDIDAD -->
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputAnchuraCaja">Anchura</label>
                            <input type="number" class="form-control " id="inputAnchuraCaja" name="AnchuraCaja" placeholder="Anchura" required min="0">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputAlturaCaja">Altura</label>
                            <input type="number" class="form-control " id="inputAlturaCaja" name="AlturaCaja" placeholder="Altura" required min="0">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputProfundidadCaja">Profundidad</label>
                            <input type="number" class="form-control " id="inputProfundidadCaja" name="ProfundidadCaja" placeholder="Profundidad" required min="0">
                        </div>
                    </div>
                    <!--MATERIAL-->
                    <div class="form-group row">
                        <label for="inputMaterialCaja" class="col-sm-2 col-form-label">Material</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control " id="inputMaterialCaja" name="MaterialCaja"  placeholder="Material" required>
                        </div>
                    </div>
                    <!--CONTENIDO-->
                    <div class="form-group row">
                        <label for="inputContenidoCaja" class="col-sm-2 col-form-label">Contenido</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputContenidoCaja" name="ContenidoCaja" placeholder="Contenido" required>
                        </div>
                    </div>
                    <?php
                    $arrayObjEstanteriasLibres = $_SESSION['EstanteriasLibres'];
                    ?>
                    <!-- ESTANTERIA, LEJA -->
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputAnchuraCaja">Estanteria</label>

                            <select class="form-control " name="selectInfoEstanteria" id="selectInfoEstanteria" onchange="muestraLejasLibres(this.value)" required>
                                <option value="null">Elige una estanteria</option>
                                <?php foreach ($arrayObjEstanteriasLibres as $objEstanteria) { ?>
                                    <option value="<?php echo $objEstanteria->idEstanteria ?>">
                                        <?php
                                        $codigoEstanteria = $objEstanteria->codigoEstanteria;
                                        $pasilloEstanteria = $objEstanteria->letra;
                                        $numeroEstanteria = $objEstanteria->numero;
                                        echo 'Estanteria: ' . $codigoEstanteria . " pasillo " . $pasilloEstanteria . " numero " . $numeroEstanteria;
                                        ?></option>
                                <?php } ?>
                            </select>

                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputLejaCaja">Leja</label>
                            <select class="form-control " name="inputLejaCaja" id="inputLejaCaja">
                                <option>Huecos</option>
                            </select>
                        </div>
                        <script src="../JavaScript/ajaxLejasLibres.js"></script>
                    </div>
                    <!--CONTENIDO-->
                    <div class="form-group row">
                        <label for="inputFechaCaja" class="col-sm-2 col-form-label">Fecha</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control " id="inputFechaCaja" name="FechaCaja" placeholder="Contenido" required>
                        </div>
                    </div>
                    <input class="btn btn-outline-primary " type="submit" id="botonEnviarAltaCaja" disabled>
                </form>

            </div>
        </section>  
        <script>
                                $('#inputCodigoCaja').keyup(function (e) {
                                    if (this.value.length < 2) {
                                        this.value = 'CA';
                                    } else if (this.value.indexOf('CA') !== 0) {
                                        this.value = 'CA' + String.fromCharCode(e.which);
                                    }
                                });
        </script>
    </body>
</html>
